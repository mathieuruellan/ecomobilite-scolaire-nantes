#!/usr/local/bin/node

const fetch = require('node-fetch');

const result = {
    "type": "FeatureCollection",
    "features": []
};

const communes_insee_codes =[... new Set([ 44018, 44172, 44190, 44009, 44143, 44150, 44171, 44194, 44074,  44094,  44114, 44024, 44026, 44035, 44101, 44162, 44166, 44215, 44020, 44047, 44109, 44120, 44198, 44204])];



const main = async function (){
    
    
    const fetchPromises = [];

    const getJsonUrl = async (url) => {
        return (await fetch(url)).json();
    }

    communes_insee_codes.forEach(code => {
        const geojsonurl = `https://data.paysdelaloire.fr/explore/dataset/234400034_etablissements-premier-et-second-degres-pdl/download/?format=geojson&disjunctive.nature_uai=true&disjunctive.nature_uai_libe=true&disjunctive.code_departement=true&disjunctive.code_region=true&disjunctive.code_academie=true&disjunctive.secteur_prive_code_type_contrat=true&disjunctive.secteur_prive_libelle_type_contrat=true&disjunctive.code_ministere=true&disjunctive.libelle_ministere=true&refine.nature_uai_libe=ECOLE+MATERNELLE&refine.nature_uai_libe=ECOLE+DE+NIVEAU+ELEMENTAIRE&refine.nature_uai_libe=ECOLE+ELEMENTAIRE+D+APPLICATION&refine.libelle_departement=Loire-Atlantique&refine.code_commune=${code}&timezone=Europe/Berlin&lang=fr`;

        fetchPromises.push(getJsonUrl(geojsonurl));
    });

    https://data.paysdelaloire.fr/explore/dataset/234400034_etablissements-premier-et-second-degres-pdl/download/?format=geojson&disjunctive.nature_uai=true&disjunctive.nature_uai_libe=true&disjunctive.code_departement=true&disjunctive.code_region=true&disjunctive.code_academie=true&disjunctive.secteur_prive_code_type_contrat=true&disjunctive.secteur_prive_libelle_type_contrat=true&disjunctive.code_ministere=true&disjunctive.libelle_ministere=true&refine.libelle_commune=Nantes&refine.nature_uai_libe=ECOLE+DE+NIVEAU+ELEMENTAIRE&refine.nature_uai_libe=ECOLE+MATERNELLE&timezone=Europe/Berlin&lang=fr
    const jsonArray = await Promise.all(fetchPromises);

    
    jsonArray.forEach(json=> {
        result.features = [...result.features, ...json.features];
    });

    console.log(`${JSON.stringify(result)}`);
};


main();