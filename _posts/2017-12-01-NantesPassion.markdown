---
layout: post
title:  "une cinquantaine (d'écoles) seront aménagées d’ici 2020 dans le cadre du plan d’écomobilité scolaire soutenu par la métropole."
date:   2017-12-01 08:00:00 +0100
categories: presse
author: Nantes Passion
link: /assets/images/articles/NP277.pdf
image: assets/images/articles/nantes-passion-decembre-2017.jpg
---

{% include image.html
            img=page.image
            link=page.image
            caption=page.title
%}

Source: [{{ page.link }}]({{ page.link }}){:rel='nofollow'}
