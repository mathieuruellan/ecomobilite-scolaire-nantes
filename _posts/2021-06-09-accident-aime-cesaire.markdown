---
layout: post
title:  "Nantes. Enfant traîné par une voiture près d’une école : « On est passé à deux doigts d’un drame »"
date:   2021-06-09 08:00:00 +0100
categories: presse
author: Yan Gauchard  • Presse Océan
link: https://www.ouest-france.fr/pays-de-la-loire/nantes-44000/nantes-enfant-traine-par-une-voiture-pres-d-une-ecole-on-est-passe-a-deux-doigts-d-un-drame-364aa2d8-c939-11eb-ac42-ca641a364e8f
image: assets/images/articles/2021-07-09-accident-aime-cesaire.png
---

{% include image.html
            img=page.image
            link=page.image
            caption=page.title
%}

Source: [{{ page.link }}]({{ page.link }}){:rel='nofollow'}
