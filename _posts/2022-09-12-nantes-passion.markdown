---
layout: post
title:  "Des rues scolaires pour plus de sécurité"
date:   2022-09-12 08:00:00 +0100
categories: presse
author: Nantes Passion
link: https://www.calameo.com/books/004590458539a16078a57
image: assets/images/articles/nantes-passion-septembre-2022.jpeg
---

{% include image.html
            img=page.image
            link=page.image
            caption=page.title
%}

Source: [{{ page.link }}]({{ page.link }}){:rel='nofollow'}
