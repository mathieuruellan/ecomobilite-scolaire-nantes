---
layout: post
title:  "Un enfant victime d’un accident près de l’école Longchamp : l’inquiétude des parents"
date:   2021-10-15 08:00:00 +0100
categories: presse
author: Virginie Meillerais et Emmanuel Vautier • Presse Océan
link: https://www.ouest-france.fr/pays-de-la-loire/nantes-44000/nantes-un-enfant-victime-d-un-accident-pres-de-l-ecole-longchamp-l-inquietude-des-parents-f13189e6-2d10-11ec-9285-f388b2ea32b0
image: assets/images/articles/OF-2021-10-15_accident_longchamp.jpg
---

{% include image.html
            img=page.image
            link=page.image
            caption=page.title
%}

Source: [{{ page.link }}]({{ page.link }}){:rel='nofollow'}
