---
layout: post
title:  "20 sur 144 écoles seulement d'ici 2022. La promesse était sur une généralisation du dispositif"
date:   2021-09-01 08:00:00 +0100
categories: presse
author: Nantes Passion
link: /assets/images/articles/NP309.pdf
image: assets/images/articles/nantes-passion-septembre-2021.jpeg
---

{% include image.html
            img=page.image
            link=page.image
            caption=page.title
%}

Source: [{{ page.link }}]({{ page.link }}){:rel='nofollow'}
