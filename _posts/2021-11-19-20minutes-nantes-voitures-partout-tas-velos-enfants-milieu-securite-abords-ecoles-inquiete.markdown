---
layout: post
title:  "Nantes : « Des voitures partout, des tas de vélos, et les enfants au milieu… » La sécurité aux abords des écoles inquiète"
date:   2021-11-19 08:00:00 +0100
categories: presse
author: 20 minutes Nantes - Julie Urbach - @julieUrbach
link: https://www.20minutes.fr/nantes/3176579-20211119-nantes-voitures-partout-tas-velos-enfants-milieu-securite-abords-ecoles-inquiete
image: assets/images/articles/2021-11-19-20minutes-nantes-voitures-partout-tas-velos-enfants-milieu-securite-abords-ecoles-inquiete.webp
---

{% include image.html
            img=page.image
            link=page.image
            caption=page.title
%}

Source: [{{ page.link }}]({{ page.link }}){:rel='nofollow'}
