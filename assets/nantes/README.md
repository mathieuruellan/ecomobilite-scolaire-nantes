#GEOJSON

Update Nantes.json to with this url:
```https://data.paysdelaloire.fr/explore/dataset/234400034_etablissements-premier-et-second-degres-pdl/download/?format=geojson&disjunctive.nature_uai=true&disjunctive.nature_uai_libe=true&disjunctive.code_departement=true&disjunctive.code_region=true&disjunctive.code_academie=true&disjunctive.secteur_prive_code_type_contrat=true&disjunctive.secteur_prive_libelle_type_contrat=true&disjunctive.code_ministere=true&disjunctive.libelle_ministere=true&refine.libelle_commune=Nantes&refine.nature_uai_libe=ECOLE+MATERNELLE&refine.nature_uai_libe=ECOLE+DE+NIVEAU+ELEMENTAIRE&timezone=Europe/Berlin&lang=fr```

