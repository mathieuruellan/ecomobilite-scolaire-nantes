---
layout: page
title: À propos
description: À propos de l'observatoire de l'éco-mobilité scolaire 
permalink: /a-propos/
image: assets/images/pages/a-propos_circulation-douce-small.jpg
toc: true
---


L'objet de ce site est d'observer la politique autour des déplacements actifs/dous sur la métropole nantaise.
Dans un premier temps, nous nous concentrons sur l'observation de la promesse n°71 du programme Nantes en confiance.

Toute bonne volonté pour alimenter ce site est la bienvenue. [Vous pouvez nous écrire ici](mailto:{{ site.email }} "contact email").
